this guide/example repo is based on https://github.com/drduh/YubiKey-Guide

see the other readme.md for the original explanation

This guide in particular is here also for other ssh uses of gpg-agent

Using gpg-agent instead of ssh-agent has some advantages, but ssh-agent is better in other ways: simpler and suports more keytypes

# Quick howtos

## creating a new private ssh key as subkey of your pgp key
this is actually wel described in Yubikey-Guide
just skip the card-status and keytocard options
## importing an existing ssh private key into gpg
### set up your gpg-agent.conf to enable-ssh-support
### restart gpg-agent if needed
* gpg-connect-agent killagent /bye
* gpg-connect-agent /bye
### ensure ssh will use gpg-agent via the environment variable
* use gpgconf --list-dirs to find out the socketfile (not gpgconf list-dirs; the -- are important)
* export SSH_AUTH_SOCK=$( gpgconf --list-dirs agent-ssh-socket )
* an advantage (or security disadvantage?) is that gpg-agent has a predictable path for this socketfile, usually something like /run/user/<uid>/gnupg/S.gpg-agent.ssh and something similar on windows, whereas ssh-agent randomizes part of the path
### once that works, simply ssh-add </path/file/to/id_rsa> (or similar)
* gpg-agent saves the contents of the privatekey file in .gnupg/private-keys-v1.d/<keyid>.key
* the file .gnupg/sshcontrol will contain the options for this <keyid> eg "confirm", lifetime parameters,... 
* you may want to remove the confirm keyword there, since gpg-agent will ask you for confirmation on every connection; pretty annoying...
* you can delete or rename the original key in .ssh/id_rsa, it will not be used anymore
* this step also allows/forces? you to put a passphrase on the key
* strangely, the sshkey is not listed as an auth subkey in gpg --list-keys.
