fixtty.sh  # if pinentry fails to launch, use this
gpg-agent.conf # various options, including enabling ssh support. copy to ~/.gnupg and restart
import_ssh-private-key.sh # importing an existing plain ssh private key
relearn_key.sh # when saving same gpg/ssh key on multiple cards, gpg-agent/pinentry will ask for the last particular card used, even though it is the same key. rerun this script every time you switch keys
restart.sh # restart gpg-agent
sshsock.sh # set the SSH_AUTH_SOCK to the one produced by gpg-agent
ssh.sh     # same, slightly different parsing 
