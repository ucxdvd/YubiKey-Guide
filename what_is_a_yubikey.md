#What
* Yubikeys are several devices/functionalities in 1 device, and some of them can be reprogrammed
* These functionalities depend on the type of yubikey: some cheaper ones only support u2f, the common ones like yubikey5nfc and varieties are mostly the same, and then there are the fips versions and "biokeys" = fingerprint 
## Typical functionalities
### u2f key: unique hardware id that you can link to your online accounts as a 2fa device.  Stored in "slot 1", and activates with short press on the button or what passes for a button (not sure on the usb-c varieties).  Yubico also offers a neat yubico authenticator app compatible with google authenticator.  The difference is that your accounts are stored on a yubico server, and need to be unlocked with your unique yubikey.
This also works via nfc.
Very recent versions of openssh 8.2p+ can use u2f keys as part of the privatekey file; both client and server will need to support this keyformat (ssh-keygen -t ecdsa-sk or ed25519-sk).
### slot2 is by default empty, and activated by "long press" (5sec or more?)  Typical uses are:
* fixed password phrase: in this case the yubikey acts like a preprogrammed usb keyboard and "types" the password for you in a password field, or into the username field or something else for anybody nearby to read :-) So best to put only part of your password in here.
* TOTP: a common OneTimePassword technology, supposedly compatible with pam.d, keepass,... not tested yet.
* some specific yubikey - windows only thing that only works for local windows accounts; the key then replaces typing a password.
### openpgp/PVID slot:
* you need to create an gnupg 'pgp' key,
* add an "authentication subkey" (this is your ssh privatekey)
* and write it to your card with the gpg toolset
* only rsa seems to be supported? so definitely use at least 4096 bits.
* you need to use gpg-agent instead of ssh-agent to use the key (required; since a yubikey exposes no files) with a supported smartcard backend to talk to the key (scdaemon, gpg4windows/kleopatra)
