#!/bin/sh

# actually very simple... once gnu-agent is your ssh-agent via the SSH_AUTH_SOCK variable, you can ssh-add /path/to/id_rsa (not tested with other cyphers)

ssh-add $1
echo "Please chech the .gnupg/sshcontrol file for the settings of this key"
